@extends('templates.basic')

@section('content')
    <div class="row">
        <div class="col-lg-3">
            @include('templates.embed.left-panel')
        </div>

        <div class="col-lg-9">
            @include('templates.embed.carousel')

            @include('templates.embed.all-products')
        </div>
    </div>
@endsection
