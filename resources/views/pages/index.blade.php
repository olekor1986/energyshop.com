@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h3 class="display-3">Страницы</h3>
            <p>Полезная информация</p>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
        @foreach($pages as $page)
            <div class="col-md-4">
                <h4>{{$page->title}}</h4>
                <p>{{$page->intro}}</p>
                <p><a class="btn btn-outline-primary" href="/pages/{{$page->id}}" role="button">Подробнее &raquo;</a></p>
                <p><a class="btn btn-outline-success" href="/pages/{{$page->id}}/edit" role="button">Редактировать &raquo;</a></p>
                <form action="/pages/{{$page->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-outline-warning">Удалить &raquo;</button>
                </form>
            </div>
        @endforeach
        </div>
    </div>
@endsection


