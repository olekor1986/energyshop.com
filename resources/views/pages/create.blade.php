@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h3 class="display-3">Добавить новую страницу</h3>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" method="post" action="/pages">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Title:
                        <input type="text" name="title" class="form-control">
                    </label>
                </div>
                <div class="form-group">
                    <label>Slug:
                        <input type="text" name="slug" class="form-control">
                    </label>
                </div>
                <div class="form-group">
                    <label>Intro:
                        <input type="text" name="intro" class="form-control">
                    </label>
                </div>
                <div class="form-group">
                    <label>Content:
                        <textarea name="content" class="form-control"></textarea>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
