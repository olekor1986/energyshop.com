@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h4 class="display-3">Редактировать статью: "{{$page->title}}"</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" method="post" action="/pages/{{$page->id}}">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Title:
                        <input type="text" name="title" class="form-control" value="{{$page->title}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Slug:
                        <input type="text" name="slug" class="form-control" value="{{$page->slug}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Intro:
                        <input type="text" name="intro" class="form-control" value="{{$page->intro}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Content:
                        <textarea name="content" class="form-control">{{$page->content}}"</textarea>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

