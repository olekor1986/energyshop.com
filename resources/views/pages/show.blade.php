@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h5 class="display-3">{{$page->title}}</h5>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <h5>{{$page->intro}}</h5>
                    <p>{{$page->content}}</p>
                </div>
        </div>
    </div>
@endsection
