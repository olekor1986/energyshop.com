@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h3 class="display-3">Категории продуктов</h3>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
        @foreach($categories as $category)
                <div class="col-md-4">
                    <h4>{{$category->name}}</h4>
                    <p><a class="btn btn-outline-primary" href="/categories/{{$category->id}}" role="button">Подробнее &raquo;</a></p>
                    <p><a class="btn btn-outline-success" href="/categories/{{$category->id}}/edit" role="button">Редактировать &raquo;</a></p>
                    <form action="/categories/{{$category->id}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-warning">Удалить &raquo;</button>
                    </form>
                </div>
        @endforeach
        </div>
    </div>
@endsection


