@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h4 class="display-3">Редактировать категорию {{$category->name}}</h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" method="post" action="/categories/{{$category->id}}">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Name:
                        <input type="text" name="name" class="form-control" value="{{$category->name}}">
                    </label>
                </div>
                <div class="form-group">
                    <label>Slug:
                        <input type="text" name="slug" class="form-control" value="{{$category->slug}}">
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

