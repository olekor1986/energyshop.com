@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h5 class="display-3">{{$category->name}}</h5>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
        @foreach($category->products as $product)
            <div class="col-md-4">
                <h4>{{$product->title}}</h4>
                <h5>{{$product->price}} грн.</h5>
                <p><a class="btn btn-outline-primary" href="/products/{{$product->id}}" role="button">Подробнее &raquo;</a></p>
                <p><a class="btn btn-outline-success" href="/products/{{$product->id}}/edit" role="button">Редактировать &raquo;</a></p>
                <form action="/products/{{$product->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-outline-warning">Удалить &raquo;</button>
                </form>
            </div>
        @endforeach
        </div>
    </div>
@endsection
