@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h5 class="display-3">{{$product->title}}</h5>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <h5>{{$product->price}} грн.</h5>
                    <p>{{$product->description}}</p>
                </div>
        </div>
        <p>
            <a class="btn btn-outline-success" href="/categories" role="button">Категории -></a>
            <a class="btn btn-outline-success" href="/category/{{$product->category->id}}" role="button">
                {{$product->category->name}} -></a>
            <a class="btn btn-outline-success" href="/products/{{$product->id}}" role="button">{{$product->title}}</a>
        </p>
    </div>
@endsection
