@extends('templates.basic')

@section('content')
    <div class="row">
        @foreach($products as $product)
        <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="/products/{{$product->id}}">{{$product->title}}</a>
                    </h4>
                    <h5>{{$product->price}} грн.</h5>
                </div>
            </div>
            @if(Auth::check())
                <p><a class="btn btn-outline-success" href="/products/{{$product->id}}/edit" role="button">Редактировать &raquo;</a></p>
                <form action="/products/{{$product->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-outline-warning">Удалить &raquo;</button>
                </form>
            @endif
        </div>
        @endforeach
    </div>
    <!-- /.row -->
@endsection


