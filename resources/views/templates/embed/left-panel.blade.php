<h3 class="my-4">Категории</h3>
<div class="list-group">
    @foreach($categories as $category)
        <a href="/categories/{{$category->id}}" class="list-group-item">{{$category->name}}</a>
    @endforeach
</div>
@if(Auth::check())
    <h1 class="my-4">Admin panel</h1>
<div class="list-group">
    <a class="list-group-item" href="/categories/create">Добавить категорию</a>
    <a class="list-group-item" href="/products/create">Добавить продукт</a>
    <a class="list-group-item" href="/pages/create">Добавить страницу</a>
</div>
@endif
