<div class="row">
    @foreach($products as $product)
        <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="/products/{{$product->id}}">{{$product->title}}</a>
                    </h4>
                    <h5>{{$product->price}} грн.</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                </div>
                @if(Auth::check())
                <div class="card-footer">
                    <a class="btn btn-sm btn-success" href="/products/{{$product->id}}/edit" role="button">Edit</a>
                    <form class="form-horizontal" action="/products/{{$product->id}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    @endforeach
</div>
<!-- /.row -->
