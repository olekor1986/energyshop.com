<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Котлы отопительные',
                'slug' => 'kotly-otopitelnie'
            ],
            [
                'name' => 'Насосы',
                'slug' => 'nasosy'
            ],
            [
                'name' => 'Запчасти для котлов',
                'slug' => 'zapchasty-dlya-kotlov'
            ],
            [
                'name' => 'Инструменты',
                'slug' => 'instrumenty'
            ]
        ]);
    }
}
