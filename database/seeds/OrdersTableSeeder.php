<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $orders = [
        [
            'customer_name' => 'Бархотов Борислав Левович',
            'email' => 'barhotov_30@mail.com',
            'phone' => '+38(060) 114-55-19',
            'feedback' => 'Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula.
                Fusce pharetra convallis urna. Mauris turpis nunc, blandit et, volutpat molestie'
        ],
        [
            'customer_name' => 'Никаноров Владислав Моисеевич',
            'email' => 'nikanorov_25@mail.com',
            'phone' => '+38(064) 383-98-18',
            'feedback' => 'Suspendisse eu ligula. Morbi mattis ullamcorper velit.
                Praesent vestibulum dapibus nibh. Donec vitae sapien ut libero venenatis faucibus. Vivamus cons'
        ],
        [
            'customer_name' => 'Кортнев Венедикт Давидович',
            'email' => 'kortnev_71@mail.com',
            'phone' => '+38(058) 286-61-41',
            'feedback' => 'Praesent adipiscing. Donec vitae orci sed dolor rutrum auctor.
                Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pel'
        ],
        [
            'customer_name' => 'Буков Иннокентий Ильевич',
            'email' => 'bukov_85@mail.com',
            'phone' => '+38(055) 673-55-36',
            'feedback' => 'Proin faucibus arcu quis ante. Praesent blandit laoreet nibh.
                Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nullam dictum'
        ],
        [
            'customer_name' => 'Грехов Афанасий Родионович',
            'email' => 'grehov_80@mail.com',
            'phone' => '+38(080) 578-52-46',
            'feedback' => 'Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu,
                nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fame'
        ],
        [
            'customer_name' => 'Ёжиков Серафим Касьянович',
            'email' => 'ezhikov_29@mail.com',
            'phone' => '+38(055) 530-85-34',
            'feedback' => 'Aenean ut eros et nisl sagittis vestibulum. Vivamus aliquet elit ac nisl.
                Curabitur a felis in nunc fringilla tristique. Phasellus ullamcorper ipsum r'
        ],
        [
            'customer_name' => 'Малютин Кир Потапович',
            'email' => 'malyutin_56@mail.com',
            'phone' => '+38(091) 772-50-18',
            'feedback' => 'Praesent congue erat at massa. Curabitur a felis in nunc fringilla tristique.
                Nam eget dui. Proin magna. Suspendisse feugiat. Ut tincidunt tincidunt e'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert($this->orders);
    }
}
