<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Route::get('/contacts', function(){
    return view('contacts.index');
});

Route::get('/products', 'ProductsController@index');

Route::get('/products/create', 'ProductsController@create');

Route::get('/products/{product}', 'ProductsController@show');

Route::post('/products', 'ProductsController@store');

Route::get('/products/{product}/edit', 'ProductsController@edit');

Route::put('/products/{product}', "ProductsController@update");

Route::delete('/products/{product}', 'ProductsController@delete');

Route::get('/pages', 'PagesController@index');

Route::get('/pages/create', 'PagesController@create');

Route::get('/pages/{page}', 'PagesController@show');

Route::post('/pages', 'PagesController@store');

Route::get('/pages/{page}/edit', 'PagesController@edit');

Route::put('/pages/{page}', 'PagesController@update');

Route::delete('/pages/{page}', 'PagesController@destroy');

Route::get('/categories', 'CategoryController@index');

Route::get('/categories/create', 'CategoryController@create');

Route::get('/categories/{category}', 'CategoryController@show');

Route::post('/categories', 'CategoryController@store');

Route::get('/categories/{category}/edit', 'CategoryController@edit');

Route::put('/categories/{category}', 'CategoryController@update');

Route::delete('/categories/{category}', 'CategoryController@destroy');


Route::resource('products', 'ProductsController');
Route::resource('category', 'CategoryController');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
