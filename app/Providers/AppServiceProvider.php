<?php

namespace App\Providers;

use App\Category;
use App\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('templates.embed.left-panel', function($view){
            $view->with('categories', Category::all());
        });
        view()->composer('templates.embed.all-products', function($view){
            $view->with('products', Product::all());
        });
    }

}
