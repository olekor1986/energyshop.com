<?php

namespace App\Http\Controllers;

use App\Http\Requests\PagesRequest;
use App\Page;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('pages.index', compact('pages'));
    }
    public function show(Page $page)
    {
        return view('pages.show', compact('page'));
    }
    public function create()
    {
        return view('pages.create');
    }
    public function store(PagesRequest $request)
    {
        Page::create($request->all());
        return redirect('/pages');
    }
    public function edit(Page $page)
    {
        return view('pages.edit', compact('page'));
    }
    public function update(PagesRequest $request, Page $page)
    {
        $page->update($request->all());
        return redirect('/pages');
    }
    public function destroy(Page $page)
    {
        $page->delete();
        return redirect('/pages');
    }
}
