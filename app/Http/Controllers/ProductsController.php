<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsRequest;
use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products', $products));
    }
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }
    public function create()
    {
        return view('products.create');
    }
    public function store(ProductsRequest $request)
    {
        Product::create($request->all());
        return redirect('/products');
    }
    public function edit(Product $product)
    {
        if(!$product){
            return redirect('/products');
        }
        return view('products.edit', compact('product'));
    }
    public function update(ProductsRequest $request, Product $product)
    {
        $product->update($request->all());
        return redirect('/products');
    }
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/products');
    }
}
