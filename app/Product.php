<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'slug', 'price', 'description'];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
